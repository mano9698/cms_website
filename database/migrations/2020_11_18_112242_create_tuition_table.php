<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTuitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuition', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('benefits_1');
            $table->string('benefits_2');
            $table->string('benefits_3');
            $table->string('tuition_img1');
            $table->string('tuition_img2');
            $table->string('tuition_img3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuition');
    }
}
