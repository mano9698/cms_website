@if(Auth::guard('super_admin')->check())
  <nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
      {{-- <div class="avatar"><img src="../img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle"></div> --}}
      <div class="title">
        <h1 class="h5">Suresh Vankar</h1>
        <p>Admin</p>
      </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
            <li><a href="#"> <i class="icon-home"></i>Home </a></li>
  <li class="active"><a href="/admin/teachers_list"> <i class="icon-grid"></i>Teachers</a></li>

            <li><a href="/admin/add_teachers"> <i class="icon-user-1"></i>Add New Teachers</a></li>
            <li><a href="/admin/inquiries_list"> <i class="icon-info"></i>Inquiries</a></li>
            <li><a href="/admin/contact_leads_list"> <i class="icon-info"></i>Contact Us Leads</a></li>
            <li><a href="/admin/change_password"> <i class="fa fa-key"></i>Change Password</a></li>

            <li><a href="/admin_logout"> <i class="icon-logout"></i>Log out </a></li>
    </ul>

  </nav>
@elseif(Auth::guard('teacher')->check())
  <nav id="sidebar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
      {{-- <div class="avatar"><img src="../img/avatar-6.jpg" alt="..." class="img-fluid rounded-circle"></div> --}}
      <div class="title">
        <h1 class="h5">{{Session::get('Teachername')}}</h1>
        <p>Teacher</p>
      </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
            <li class="active"><a href="#"> <i class="icon-home"></i>Home </a></li>
  <li><a href="/teachers/templates"> <i class="icon-website"></i>Website Template</a></li>
  <li><a href="/teachers/banner"> <i class="icon-picture"></i>Website Banner</a></li>
  <li><a href="/teachers/announcement"> <i class="icon-list-1"></i>Announcement</a></li>
            <li><a href="/teachers/personal_details"> <i class="icon-padnote"></i>Personal Details</a></li>
            <li><a href="/teachers/tuition"> <i class="icon-writing-whiteboard"></i>Tuition details</a></li>
  <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-layers"></i>Blog</a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="/teachers/add_blogs">Add Blog</a></li>
                <li><a href="/teachers/blogs_list">Blog List</a></li>
              </ul>
            </li>
  <li><a href="/teachers/youtube_links"> <i class="icon-presentation-1"></i>Upload My Videos</a></li>
  <li><a href="/teachers/inquiries_list"> <i class="icon-info"></i>Inquiries</a></li>
  <!--<li><a href="user.html"> <i class="icon-user-1"></i>Edit Profile</a></li>
            <li><a href="password.html"> <i class="fa fa-key"></i>Change Password</a></li>

            <li><a href="../login.html"> <i class="icon-logout"></i>Log out </a></li>-->
    </ul>

  </nav>
@endif
