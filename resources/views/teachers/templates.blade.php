@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Select Your Theme</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Select template and Click on the image to Preview</div>
              <div class="block-body">

                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" action="/teachers/templates/add_template_selection" method="POST">
                  @csrf
                <div class="form-group row">
                    
                    <div class="col-sm-12">
                      <div class="row">
                    <div class="col-md-4">
                      @if($TemplateSelection->template_id == 1)
                        <input id="radioCustom1" type="radio" value="1" name="template_id" class="radio-template" checked >
                      @else
                      <input id="radioCustom1" type="radio" value="1" name="template_id" class="radio-template" >
                      @endif
                        <label for="radioCustom1"><a target="_blank" href="/teachers/templates/1/preview"><img src="{{URL::asset('UI/img/template/1.jpg')}}"></a></label>

                        <a href="/teachers/templates/1/preview" style="width: 100%;
text-align: center;">Preview</a>
{{--                         
                        <br>
                        <a href="" class="text-center">Preview</a>
                        <br> --}}
                    </div>
                    <div class="col-md-4">
                      @if($TemplateSelection->template_id == 2)
                        <input id="radioCustom1" type="radio" value="2" name="template_id" class="radio-template" checked>
                      @else
                      <input id="radioCustom1" type="radio" value="2" name="template_id" class="radio-template">
                      @endif
                        <label for="radioCustom1"><a target="_blank" href="/teachers/templates/2/preview"><img src="{{URL::asset('UI/img/template/2.jpg')}}"></a></label>
                      
                        <a href="/teachers/templates/2/preview" style="width: 100%;
text-align: center;">Preview</a>
                    </div>
<div class="col-md-4">
                      
  @if($TemplateSelection->template_id == 3)
                        <input id="radioCustom1" type="radio" value="3" name="template_id" class="radio-template"checked >
                        @else
                        <input id="radioCustom1" type="radio" value="3" name="template_id" class="radio-template" >
                        @endif
                        <label for="radioCustom1"><a target="_blank" href="/teachers/templates/3/preview"><img src="{{URL::asset('UI/img/template/3.jpg')}}"></a></label>
                      
                        <a href="/teachers/templates/3/preview" style="width: 100%;
text-align: center;">Preview</a>

                    </div>  <div class="col-md-4">
                      @if($TemplateSelection->template_id == 4)
                        <input id="radioCustom1" type="radio" value="4" name="template_id" class="radio-template" checked>
                      @else
                      <input id="radioCustom1" type="radio" value="4" name="template_id" class="radio-template">
                      @endif

                        <label for="radioCustom1"><a target="_blank" href="/teachers/templates/4/preview"><img src="{{URL::asset('UI/img/template/4.jpg')}}"></a></label>
                      
                        <a href="/teachers/templates/4/preview" style="width: 100%;
text-align: center;">Preview</a>

                    </div>
                      <div class="col-md-4">
                      @if($TemplateSelection->template_id == 5)
                        <input id="radioCustom1" type="radio" value="5" name="template_id" class="radio-template" checked>
                      @else
                      <input id="radioCustom1" type="radio" value="5" name="template_id" class="radio-template">
                      @endif
                        <label for="radioCustom1"><a target="_blank" href="/teachers/templates/5/preview"><img src="{{URL::asset('UI/img/template/5.jpg')}}"></a></label>
                      
                        <a href="/teachers/templates/5/preview" style="width: 100%;
text-align: center;">Preview</a>

                    </div>
                      <div class="col-md-4">
                        @if($TemplateSelection->template_id == 6)
                        <input id="radioCustom1" type="radio" value="6" name="template_id" class="radio-template" checked>
                        @else
                        <input id="radioCustom1" type="radio" value="6" name="template_id" class="radio-template">
                        @endif

                        <label for="radioCustom1"><a target="_blank" href="/teachers/templates/6/preview"><img src="{{URL::asset('UI/img/template/6.jpg')}}"></a></label>
                      
                        <a href="/teachers/templates/6/preview" style="width: 100%;
text-align: center;">Preview</a>

                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="line"></div>                        
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Make my website now</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection