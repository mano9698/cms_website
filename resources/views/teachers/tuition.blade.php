@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit My Tuition on Website</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Updating My Tuition Section</strong></div>
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/teachers/store_or_update_tuitions" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Benefits 1</label>
                    <div class="col-sm-6">
                        
                        <textarea type="text" value="" class="form-control" name="benefits_1"> @if(isset($Tuitions->benefits_1)) {{$Tuitions->benefits_1}} @else @endif</textarea>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Benefits 2</label>
                    <div class="col-sm-6">
                      <textarea type="text" value="" class="form-control" name="benefits_2">@if(isset($Tuitions->benefits_2)) {{$Tuitions->benefits_2}} @else @endif</textarea>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Benefits 3</label>
                    <div class="col-sm-6">
                      <textarea type="text" value="" class="form-control" name="benefits_3">@if(isset($Tuitions->benefits_3)) {{$Tuitions->benefits_3}} @else @endif</textarea>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Tuition Image 1</label>
                    <div class="col-sm-6">
                      <input type="file" class="form-control" value="" id="tuition-img1" name="tuition_img1">

                      <div id="" class="image-previewer" data-cropzee="tuition-img1"></div>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Tuition Image 1</label>
                    <div class="col-sm-6">
                      <input type="file" class="form-control" value="" id="tuition-img2" name="tuition_img2">

                      <div id="" class="image-previewer" data-cropzee="tuition-img2"></div>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Tuition Image 1</label>
                    <div class="col-sm-6">
                      <input type="file" class="form-control" value="" id="tuition-img3" name="tuition_img3">

                      <div id="" class="image-previewer" data-cropzee="tuition-img3"></div>
                    </div>
                  </div>
                  <div class="line"></div>        
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
  <script>
    $(document).ready(function(){
      $("#tuition-img1").cropzee({startSize: [85, 85, '%'],});

      $("#tuition-img2").cropzee({startSize: [85, 85, '%'],});

      $("#tuition-img3").cropzee({startSize: [85, 85, '%'],});
    });
  </script>
  @endsection