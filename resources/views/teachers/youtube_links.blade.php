@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit My Videos On Website</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Updating My Videos</strong></div>
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" action="/teachers/add_or_update_yt_links" method="post">
                    @csrf                  
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Video Link</label>
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-md-4">
                            <input type="text" placeholder="Enter Youtube Link" value="@if(isset($$YoutubeLinks->yt_link1)) {{$YoutubeLinks->yt_link1}} @endif" name="yt_link1" class="form-control">
                        </div>
                        <div class="col-md-4">
                          <input type="text" placeholder="Enter Youtube Link" value="@if(isset($$YoutubeLinks->yt_link2)) {{$YoutubeLinks->yt_link2}} @endif" name="yt_link2" class="form-control">
                        </div>
                        <div class="col-md-4">
                          <input type="text" placeholder="Enter Youtube Link" value="@if(isset($$YoutubeLinks->yt_link3)) {{$YoutubeLinks->yt_link3}} @endif" name="yt_link3" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div> 
                  <div class="line"></div> 
 
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection