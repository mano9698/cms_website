<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Ecology Theme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Education - Welcome to Teacher Page</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/bootstrap.min.css')}}">
    <!-- Font awsome CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/font-awesome.min.css')}}">    
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/magnific-popup.css')}}">    
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.theme.css')}}">     
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/animate.css')}}"> 
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/slick.css')}}">  
   
    <!-- Mean Menu-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/meanmenu.css')}}">
    <!-- main style-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/responsive.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/demo.css')}}">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="header_tow header_inner blog_page">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>    
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="info_wrapper">
                        <div class="contact_info">                   
        					                  
                        </div>
                        <div class="login_info">
                             {{-- <ul class="d-flex">                                
                                <li class="nav-item"><a href="#" class="nav-link join_now js-modal-show"><i class="flaticon-padlock"></i>Log In</a></li>
                            </ul> --}}
                            <a href="/teachers/templates/contact/{{$Users->slug}}" title="" class="apply_btn">Book a Demo Class</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="edu_nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-faded">
                {{-- <a class="navbar-brand" href="#"><img src="images/logo2.png" alt="logo"></a> --}}
                <a class="navbar-brand" href="#">Logo</a>
                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav nav lavalamp ml-auto menu">
                        {{-- <li class="nav-item"><a href="#" class="nav-link active">Home</a>
                        </li>              --}}

                    </ul>
                </div>
            </nav><!-- END NAVBAR -->
        </div> 
    </div>

    <div class="intro_wrapper">
        <div class="container">  
            <div class="row">        
                 <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="intro_text">
                        <h1>Blog Detail</h1>
                        <div class="pages_links">
                            <a href="#" title="">Home</a>
                            <a href="#" title="" class="active">Blog Detail</a>
                        </div>
                    </div>
                </div>              

            </div>
        </div> 
    </div> 
</header><!--  End header section-->




<section class="login_signup_option">
    <div class="l-modal is-hidden--off-flow js-modal-shopify">
        <div class="l-modal__shadow js-modal-hide"></div>
        <div class="login_popup login_modal_body">
            <div class="Popup_title d-flex justify-content-between">
                <h2 class="hidden">&nbsp;</h2>
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-lg-12 login_option_btn">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#login" role="tab">Login</a></li>
                            <!--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#panel2" role="tab">Register</a></li>-->
                        </ul>
                    </div>
                    <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                        <!-- Tab panels -->
                        <div class="tab-content card">
                            <!--Login-->
                            <div class="tab-pane fade in show active" id="login" role="tabpanel">
                                <form action="#">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="email" class="form-control" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12 d-flex justify-content-between login_option">
                                            <a href="#" title="" class="forget_pass">Forget Password ?</a>
                                            <button type="submit" class="btn btn-default login_btn">Login</button>
                                        </div> 
                                    </div>
                                </form>
                            </div>
                            <!--/.Panel 1-->
                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel2" role="tabpanel">
                                <form action="#" class="register">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Name</label>
                                                <input type="text" class="form-control" placeholder="Username">
                                            </div>
                                        </div>                                        
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Email</label>
                                                <input type="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12 d-flex justify-content-between login_option">
                                            <button type="submit" class="btn btn-default login_btn">Register</button>
                                        </div> 
                                    </div>
                                </form>
                            </div><!--/.Panel 2-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  <!-- End Login Signup Option -->


<section class="blog_wrapper">
    <div class="container">  
        <div class="row">        
            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="blog_post">
                    <h3>{{$Blogs->blog_title}}</h3>
                    <div class="post_by d-flex">
                        <span>By - <a href="#" title="" class="bloger_name">{{$Users->name}}</a></span>
                        <span>Posted On : {{date('M d Y', strtotime($Blogs->date))}} </span>                        

                    </div>
                    <img src="/UI/blog_img/{{$Blogs->blog_img}}" alt="" class="img-fluid">
                    <div class="postpage_content_wrapper">
                        <div class="social_wrapper">
                            <h4>Share</h4>
                            <ul class="social_items list-unstyled">
                                <li><a href="#"><i class="fab fa-facebook-f fb_icon"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter tw_icon"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in link_icon"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram in_icon"></i></a></li>
                            </ul>
                        </div>
                        <div class="blog_post_content">
                            {{-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est</p>
                            <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est</p>
                            <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                            <h3>An Additional Point</h3>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est</p>
                            <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p> --}}

                            {!!$Blogs->description!!}
                            <div class="post_response_count d-flex justify-content-between">
                                {{-- <ul class="d-flex tags">
                                    <li class="tag_headline">Tags : </li>
                                    <li><a href="#" title="">Piterson, </a></li>
                                    <li><a href="#" title="">Cavien, </a></li>
                                    <li><a href="#" title="">human, </a></li>                    
                                    <li><a href="#" title="">master, </a></li>
                                    <li><a href="#" title="">pleasure</a></li>
                                </ul> --}}
                                {{-- <ul class="d-flex comnent_count">
                                    <li><i class="fas fa-thumbs-up"></i><a href="">3 Likes</a></li>
                                    <li><i class="fas fa-comment"></i><a href="">2 Comments</a></li>
                                </ul>   --}}
                            </div>
                    
                            <!-- Blog Comment Wrappper-->
                            {{-- <div class="commnet-wrapper">
                                <div class="items_title">
                                    <h3 class="title">2 Comments</h3>
                                </div>
                                 <div class="comment-list-items">
                                    <div class="comment-list-wrapper">
                                        <div class="comment-list">
                                            <div class="commnet_img">
                                                <img src="images/team/team_1.jpg" alt="member img" class="img-fluid">
                                            </div>
                                            <div class="comment-text">
                                                <div class="author_info"> 
                                                    <div class="author_name">
                                                        <a href="#" class="">Adam Smith</a> 
                                                        <span>20 July 2020 at 10.45 AM</span>
                                                     </div>
                                                     <div class="reply-comment">
                                                        <a href="#" title=""> <i class="flaticon-reply-arrow"></i> Reply</a>
                                                    </div> 
                                                </div>     
                                                <p>You need to be sure there isn't anything embarrassing hidden in the repeat predefined chunks as nessing hidden in the repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                                            </div>
                                        </div>

                                        <div class="comment-list reply_comment_text">
                                            <div class="commnet_img">
                                               <img src="images/team/team_3.jpg" alt="member img" class="img-fluid">
                                            </div>
                                            <div class="comment-text">
                                                <div class="author_info"> 
                                                    <div class="author_name">
                                                        <a href="#" class="">Jonson Park</a> 
                                                        <span>25 July 2020 at 10.45 AM</span>
                                                     </div>
                                                     <div class="reply-comment">
                                                        <a href="#" title=""> <i class="flaticon-reply-arrow"></i> Reply</a>
                                                    </div> 
                                                </div>     
                                                <p>You need to be sure there isn't anything embarrassing hidden in the repe essary, making this the first true generator on the Internet.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment-list-wrapper">
                                        <div class="comment-list">
                                            <div class="commnet_img">
                                                <img src="images/team/team_2.jpg" alt="member img" class="img-fluid">
                                            </div>
                                            <div class="comment-text">
                                                <div class="author_info"> 
                                                    <div class="author_name">
                                                        <a href="#" class="">Jonathon Smith</a> 
                                                        <span>09 July 2020 at 10.45 AM</span>
                                                     </div>
                                                     <div class="reply-comment">
                                                        <a href="#" title=""> <i class="flaticon-reply-arrow"></i> Reply</a>
                                                    </div> 
                                                </div>     
                                                <p>You need to be sure there isn't anything embarrassing hidden in the repeat predefined chunks as nessing hidden in the repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                                            </div>                               
                                        </div>
                                    </div>
                                </div> 
                                <!--  Leave Commnent Wrapper -->
                                <div class="leave_comment_wrapper">
                                    <div class="items_title">
                                        <h3 class="title">Leave A Comment</h3>
                                        <p>Please enter your queries</p>
                                    </div>
                                    <div class="leave_comment">
                                        <div class="contact_form">
                                            <form action="#">
                                                <div class="row">
                                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                                       <input type="text" class="form-control" id="name"  placeholder="Name">
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                                        <input type="email" class="form-control" id="email" placeholder="Email">
                                                    </div>
                                                    <div class="col-12 col-sm-12 col-md-12 form-group">
                                                        <textarea class="form-control" id="comment" placeholder="Your Comment Wite Here ..."></textarea>
                                                    </div>


                                                     <div class="col-12 col-sm-12 col-md-12 submit-btn">
                                                        <button type="submit" class="text-center">Post Comment</button>
                                                    </div>
                                                </div>
                                            </form>   
                                        </div>
                                    </div> 
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>               
            </div> <!-- End Blog Left Side-->

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 blog_wrapper_right ">
                <div class="blog-right-items">

                    <div class="become_a_teacher widget_single">
                        <div class="form-full-box">
                            <div class="form_title">
                                <h2>Become A Student</h2>
                                <p>Get Access to <span>20+ </span>Video courses </p>
                            </div>
                            <form>
                                <div class="register-form">
                                    <div class="row">
                                        <div class="col-12 col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label><i class="fas fa-user"></i></label>
                                                <input class="form-control" name="name" placeholder="Write Your Name" required="" type="text">
                                            </div>
                                        </div>

                                        <div class="col-12 col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <label><i class="flaticon-email"></i></label>
                                                <input class="form-control" name="email" placeholder="Write Your E-mail" required="" type="email">
                                            </div>
                                        </div>
                                        <div class="col-12 col-xs-12 col-md-12">
                                            <div class="form-group massage_text">
                                                <label><i class="flaticon-copywriting"></i></label>
                                                <textarea class="form-control"  placeholder="Write Something Here" required="" ></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12 col-xs-12 col-md-12 register-btn-box">
                                            <button class="register-btn" type="submit">Send Now</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <div class="recent_post_wrapper widget_single">
                        <div class="items-title">
                            <h3 class="title">Recent Blogs</h3>
                        </div>
                        @if(count($RecentBlogs) != 0)
                            @foreach($RecentBlogs as $Blog)
                            <div class="single-post">
                                <div class="recent_img">
                                    <a href="#" title=""><img src="images/blog/side_blog_1.jpg" alt="" class="img-fluid"></a>
                                </div>
                                <div class="post_title">
                                    <a href="#" title="">{{$Blog->blog_title}}</a>
                                    <div class="post-date">
                                        <span>{{date('M d Y', strtotime($Blog->date))}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <h4 class="text-center">No Blogs Found.</h4>
                        @endif
                    </div>

                    
                </div>
            </div><!-- ./ End  Blog Right Side-->
            
        </div>
    </div> 
</section> <!-- ./ End Blog Area Wrapper-->


<!-- Footer -->  
<footer class="footer_2 bgfooter">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                 <div class="col-12 col-md-12 col-lg-12">
                    <div class="copyright">Copyright &copy;2020 All rights reserved | Design is made with by 
                        <a target="_blank" href="https://www.techitalents.com">Techitalents</a>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="shapes_bg">
        <img src="images/shapes/footer_2.png" alt="" class="shape_1">
    </div>    
</footer><!-- End Footer -->

<section id="scroll-top" class="scroll-top">
    <h2 class="disabled">Scroll to top</h2>
    <div class="to-top pos-rtive">
        <a href="#"><i class = "flaticon-right-arrow"></i></a>
    </div>
</section>

   <!-- JavaScript -->
   <script src="{{URL::asset('UI/themes/js/jquery-3.2.1.min.js')}}"></script>
   <script src="{{URL::asset('UI/themes/js/popper.min.js')}}"></script>
   <script src="{{URL::asset('UI/themes/js/bootstrap.min.js')}}"></script>
   <!-- Revolution Slider -->
   <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.revolution.min.js')}}"></script>
   <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.tools.min.js')}}"></script> 
   <script src="{{URL::asset('UI/themes/js/jquery.magnific-popup.min.js')}}"></script>     
   <script src="{{URL::asset('UI/themes/js/owl.carousel.min.js')}}"></script>   
   <script src="{{URL::asset('UI/themes/js/slick.min.js')}}"></script>   
   <script src="{{URL::asset('UI/themes/js/jquery.meanmenu.min.js')}}"></script>   
   <script src="{{URL::asset('UI/themes/js/wow.min.js')}}"></script> 
   <!-- Counter Script -->
   <script src="{{URL::asset('UI/themes/js/waypoints.min.js')}}"></script>
   <script src="{{URL::asset('UI/themes/js/jquery.counterup.min.js')}}"></script>

   <!-- Revolution Extensions -->
   <script src="{{URL::asset('UI/themes/js/custom.js')}}"></script>  
    
</body>
</html>
