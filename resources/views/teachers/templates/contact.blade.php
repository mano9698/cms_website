<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Ecology Theme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Education - Welcome to Teacher Page</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/bootstrap.min.css')}}">
    <!-- Font awsome CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/font-awesome.min.css')}}">    
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/magnific-popup.css')}}">    
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.theme.css')}}">     
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/animate.css')}}"> 
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/slick.css')}}">  
   
    <!-- Mean Menu-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/meanmenu.css')}}">
    <!-- main style-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/responsive.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/demo.css')}}">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="header_tow header_inner contact_page">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>    
    <!--<div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="info_wrapper">
                        <div class="contact_info contact_marquee">                   
                             <p>“Tell me and I forget, teach me and I may remember and I learn.”</p>
                        </div>
                        <div class="login_info">
                             <ul class="d-flex">                                
                                <li class="nav-item"><a href="#" class="nav-link join_now js-modal-show"><i class="flaticon-padlock"></i>Log In</a></li>
                            </ul>
                            <a href="#" title="" class="apply_btn">Contact Me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <div class="edu_nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-faded">
                
               
            </nav><!-- END NAVBAR -->
        </div> 
    </div>

    <div class="intro_wrapper">
        <div class="container">  
            <div class="row">        
                 <div class="col-sm-12 col-md-8 col-lg-8">

                </div>              

            </div>
        </div> 
    </div> 
</header><!--  End header section-->







<section class="contact_info_wrapper">
     <div class="container">  
        <div class="row">  
            <div class="col-12 col-sm-12 col-md-12 col-lg-3"></div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="contact_form_wrapper">
                    <h3 class="title">Contact Form</h3>
                    <div class="leave_comment">
                        <div class="contact_form">
                            @if(session('message'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{!! session('message') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            <form action="/teachers/templates/add_contact" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6 form-group">

                                        <input type="hidden" value="{{$Users->id}}" name="user_id">

                                       <input type="text" class="form-control" id="name"  placeholder="Your Name" name="name">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                        <input type="email" class="form-control" id="email" placeholder="Your E-mail" name="email">
                                    </div> 
                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                       <input type="text" class="form-control" id="name"  placeholder="Your Contact No" name="contact">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                        <input type="text" class="form-control" id="email" placeholder="Your Whatsapp No" name="whatsapp_no">
                                    </div>   
                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                       <input type="text" class="form-control" id="name"  placeholder="Your Country" name="country">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 form-group">
                                        <input type="text" class="form-control" id="email" placeholder="Your City & State" name="state_city">
                                    </div>                                 
                                    <div class="col-12 col-sm-12 col-md-12 form-group">
                                        <textarea class="form-control" id="comment" placeholder="Your Requirement Details ..." name="requirements"></textarea>
                                    </div>
                                     <div class="col-12 col-sm-12 col-md-12 submit-btn" style="text-align: center;">
                                        <button type="submit" class="text-center">Submit Form</button>
                                    </div>
                                </div>
                            </form>   
                        </div>
                    </div> 
                </div>
           </div>
           <div class="col-12 col-sm-12 col-md-12 col-lg-3"></div>
        </div>
    </div>
</section> <!-- Contact Info Wrappper-->


<!-- Footer -->  
<footer class="footer_2 bgfooter">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                 <div class="col-12 col-md-12 col-lg-12">
                    <div class="copyright">Copyright &copy;2020 All rights reserved | Design is made with by 
                        <a target="_blank" href="https://www.techitalents.com">Techitalents</a>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="shapes_bg">
        <img src="images/shapes/footer_2.png" alt="" class="shape_1">
    </div>    
</footer><!-- End Footer -->

<section id="scroll-top" class="scroll-top">
    <h2 class="disabled">Scroll to top</h2>
    <div class="to-top pos-rtive">
        <a href="#"><i class = "flaticon-right-arrow"></i></a>
    </div>
</section>

    <!-- JavaScript -->
    <script src="{{URL::asset('UI/themes/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/popper.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/bootstrap.min.js')}}"></script>
    <!-- Revolution Slider -->
    <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.tools.min.js')}}"></script> 
    <script src="{{URL::asset('UI/themes/js/jquery.magnific-popup.min.js')}}"></script>     
    <script src="{{URL::asset('UI/themes/js/owl.carousel.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/slick.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/jquery.meanmenu.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/wow.min.js')}}"></script> 
    <!-- Counter Script -->
    <script src="{{URL::asset('UI/themes/js/waypoints.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/jquery.counterup.min.js')}}"></script>

    <!-- Revolution Extensions -->
    <script src="{{URL::asset('UI/themes/js/custom.js')}}"></script>  
    
</body>
</html>
