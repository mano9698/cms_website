<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Ecology Theme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Education - Welcome to Teacher Page</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/bootstrap.min.css')}}">
    <!-- Font awsome CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/font-awesome.min.css')}}">    
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/magnific-popup.css')}}">    
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.theme.css')}}">     
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/animate.css')}}"> 
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/slick.css')}}">  
   
    <!-- Mean Menu-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/meanmenu.css')}}">
    <!-- main style-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/responsive.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/demo.css')}}">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="header_tow">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>    
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="info_wrapper">
                        <div class="contact_info contact_marquee">   
                            @if(isset($Users->announcement))    
                                <p>{{$Users->announcement}}</p>   
                             @else
                                <p>“Tell me and I forget, teach me and I may remember and I learn.”</p>   
                             @endif
                        </div>
                        <div class="login_info">
                             {{-- <ul class="d-flex">                                 --}}
                                {{-- <li class="nav-item"><a href="#" class="nav-link join_now js-modal-show"><i class="flaticon-padlock"></i>Log In</a></li>
                            </ul> --}}
                            <a href="/teachers/templates/contact/{{$Users->slug}}" title="" class="apply_btn">Contact Me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="edu_nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-faded">
                {{-- <a class="navbar-brand" href="#"><img src="{{URL::asset('UI/themes/images/logo2.png')}}" alt="logo"></a> --}}
                <a class="navbar-brand" href="#">{{$Users->name}}</a>
                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav nav lavalamp ml-auto menu">
                        {{-- <li class="nav-item"><a href="#" class="nav-link active">Home</a>
                        </li>              --}}

                    </ul>
                </div>
            </nav><!-- END NAVBAR -->
        </div> 
    </div>


<!-- ======= Hero Section ======= -->
  <section id="hero">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

      <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

      <div class="carousel-inner" role="listbox">
        @if($Banners != null)
                <!-- Slide 1 -->
                <div class="carousel-item active" style="background-image: url('/UI/banners/{{$Banners->banner1}}')">
                    <div class="carousel-container">
                    </div>
                </div>
        
                <!-- Slide 2 -->
                <div class="carousel-item" style="background-image: url('/UI/banners/{{$Banners->banner2}}')">
                    <div class="carousel-container">
                    </div>
                </div>
        
                <!-- Slide 3 -->
                <div class="carousel-item" style="background-image: url('/UI/banners/{{$Banners->banner3}}')">
                    <div class="carousel-container">
                    </div>
                </div>
        @else
            <!-- Slide 1 -->
            <div class="carousel-item active" style="background-image: url({{URL::asset('UI/themes/images/slide/slide-1.jpg')}})">
                <div class="carousel-container">
                </div>
            </div>

            <!-- Slide 2 -->
            <div class="carousel-item" style="background-image: url({{URL::asset('UI/themes/images/slide/slide-2.jpg')}})">
                <div class="carousel-container">
                </div>
            </div>

            <!-- Slide 3 -->
            <div class="carousel-item" style="background-image: url({{URL::asset('UI/themes/images/slide/slide-3.jpg')}})">
                <div class="carousel-container">
                </div>
            </div>
        @endif
        

      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon fas fa-arrow-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon fas fa-arrow-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->
</header><!--  End header section-->




<section class="login_signup_option">
    <div class="l-modal is-hidden--off-flow js-modal-shopify">
        <div class="l-modal__shadow js-modal-hide"></div>
        <div class="login_popup login_modal_body">
            <div class="Popup_title d-flex justify-content-between">
                <h2 class="hidden">&nbsp;</h2>
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-lg-12 login_option_btn">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#login" role="tab">Login</a></li>
                            <!--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#panel2" role="tab">Register</a></li>-->
                        </ul>
                    </div>
                    <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                        <!-- Tab panels -->
                        <div class="tab-content card">
                            <!--Login-->
                            <div class="tab-pane fade in show active" id="login" role="tabpanel">
                                <form action="#">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="email" class="form-control" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12 d-flex justify-content-between login_option">
                                            <a href="#" title="" class="forget_pass">Forget Password ?</a>
                                            <button type="submit" class="btn btn-default login_btn">Login</button>
                                        </div> 
                                    </div>
                                </form>
                            </div>
                            <!--/.Panel 1-->
                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel2" role="tabpanel">
                                <form action="#" class="register">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Name</label>
                                                <input type="text" class="form-control" placeholder="Username">
                                            </div>
                                        </div>                                        
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Email</label>
                                                <input type="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12 d-flex justify-content-between login_option">
                                            <button type="submit" class="btn btn-default login_btn">Register</button>
                                        </div> 
                                    </div>
                                </form>
                            </div><!--/.Panel 2-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  <!-- End Login Signup Option -->



<section class="teachers_profile">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 teacher-detail-left">
                <div class="teacher_info_wrapper">
                    <div class="teacger-image">
                        @if(isset($Users->profile_pic))
                            <img src="/UI/profile_pic/{{$Users->profile_pic}}" alt="" class="img-fluid">
                        @else
                            <img src="{{URL::asset('UI/themes/images/team/team_7.jpg')}}" alt="" class="img-fluid">
                        @endif
                    </div>
                    <div class="social_wraper">
                        <ul class="social-items d-flex list-unstyled">
                            <li><a href="{{$Users->facebook}}" target="_blank"><i class="fab fa-facebook-f fb-icon"></i></a></li>
                            <li><a href="{{$Users->twitter}}" target="_blank"><i class="fab fa-twitter twitt-icon"></i></a></li>
                            <li><a href="{{$Users->linkedin}}" target="_blank"><i class="fab fa-linkedin-in link-icon"></i></a></li>
                            <li><a href="{{$Users->instagram}}" target="_blank"><i class="fab fa-instagram ins-icon"></i></a></li>
                        </ul>
                    </div> 
                </div>
            </div><!-- Ends: .teacher-detail-left -->
            <div class="col-sm-8 teacher-detail-right">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="teacher-info">
                            @if(isset($Users))
                                <ul class="list-unstyled">
                                    <li>
                                        <h3>Name :</h3>
                                        <span>{{$Users->name}}</span>
                                    </li>
                                    <li>
                                        <h3>Email Address :</h3>
                                        <span>{{$Users->email}}</span>
                                    </li>
                                    <li>
                                        <h3>Experience :</h3>
                                        <span>{{$Users->exp}}</span>
                                    </li>
                                    <li>
                                        <h3>Contact Number :</h3>
                                        <span>{{$Users->contact}}</span>
                                    </li>
                                    <li>
                                        <h3>Qualification :</h3>
                                        <span>{{$Users->qualification}}</span>
                                    </li>
                                    <li>
                                        <h3>City/State/Country :</h3>
                                        <span>{{$Users->city}}/{{$Users->state}}/{{$Users->country}}</span>
                                    </li>
                                </ul>
                            @else
                                <ul class="list-unstyled">
                                    <li>
                                        <h3>Name :</h3>
                                        <span>John Doe</span>
                                    </li>
                                    <li>
                                        <h3>Email Address :</h3>
                                        <span>John.doe@gmail.com</span>
                                    </li>
                                    
                                    <li>
                                        <h3>Experience :</h3>
                                        <span>5 Years of Teaching Career</span>
                                    </li>
                                    <li>
                                        <h3>Contact Number :</h3>
                                        <span>88XXXXXXXX</span>
                                    </li>
                                    <li>
                                        <h3>Qualification :</h3>
                                        <span>M.Sc, M.Phil</span>
                                    </li>
                                    <li>
                                        <h3>City/State/Country :</h3>
                                        <span>Bangalore/KA/India</span>
                                    </li>
                                </ul>
                            @endif
                            
                        </div>
                    </div>
                  <div class="col-sm-6">
                        <div class="teacher-info">
                            <ul class="list-unstyled">
                                @if(isset($Users->about_me))
                                    <li>
                                        <h3>Few lines about me :</h3>
                                        <span>{{$Users->about_me}}</span>
                                    </li>
                                @else
                                <li>
                                    <h3>Few lines about me :</h3>
                                    <span>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip, quis nostrud exercitation.sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip, quis nostrud exercitation. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip, quis nostrud exercitation.</span>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- Ends: .teacher-detail-right -->
        </div>
    </div>
</section><!-- Ends: .teacher-details-wrapper -->



<section class="learn_shep">
    @if(isset($Tuitions))
        <div class="container">            
            <div class="row about_us_thinking">
                <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                                    <div class="title">
                        <h2>Why to join my Tuitions</h2>
                        <div class="step_services">
                            <h3><span>01.</span>Benefits</h3>
                            <p>{{$Tuitions->benefits_1}}</p>  
                        </div>                    
                        <div class="step_services">
                            <h3><span>02.</span>Benefits</h3>
                            <p>{{$Tuitions->benefits_2}}</p>   
                        </div>                   
                        <div class="step_services">
                            <h3><span>03.</span>Benefits</h3>
                            <p>{{$Tuitions->benefits_3}}</p>  
                        </div>
                        <a href="/teachers/templates/contact/{{$Users->slug}}" title="" class="apply_btn">Contact Me</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">            
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-7 ml-auto p-0">
                    <div class="shep_banner_wrapper">
                        <div class="step_single_banner">
                            <img src="/UI/tuitions_img/{{$Tuitions->tuition_img1}}" alt="" class="img-fluid">
                            <img src="/UI/tuitions_img/{{$Tuitions->tuition_img2}}" alt="" class="img-fluid">
                        </div>
                        <div class="step_single_banner">
                            <img src="/UI/tuitions_img/{{$Tuitions->tuition_img3}}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container">            
            <div class="row about_us_thinking">
                <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                                    <div class="title">
                        <h2>Why to join my Tuitions</h2>
                        <div class="step_services">
                            <h3><span>01.</span>Benefits</h3>
                            <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra. At de phasellus eget maecenas.</p>  
                        </div>                    
                        <div class="step_services">
                            <h3><span>02.</span>Benefits</h3>
                            <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra.  phasellus eget maecenas.</p>   
                        </div>                   
                        <div class="step_services">
                            <h3><span>03.</span>Benefits</h3>
                            <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra. Pede phasellus eget maecenas.</p>  
                        </div>
                        <a href="contact.html" title="" class="apply_btn">Contact Me</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">            
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-7 ml-auto p-0">
                    <div class="shep_banner_wrapper">
                        <div class="step_single_banner">
                            <img src="{{URL::asset('UI/themes/images/features/features_2_1.jpg')}}" alt="" class="img-fluid">
                            <img src="{{URL::asset('UI/themes/images/features/features_2_2.jpg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="step_single_banner">
                            <img src="{{URL::asset('UI/themes/images/features/features_2_3.jpg')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
   
</section><!-- End Larnign Step -->

<div id="events" class="events-area events-bg-height py-5" style="background-image: url({{URL::asset('UI/themes/images/courses_bg.png')}})">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <div class="section-title mb-5 text-center">
                        <div class="section-title-heading mb-2">
                            <h1 class="white-color">General information and updates</h1>
                        </div>
                        <div class="section-title-para">
                            <p class="pink-color">Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scelerisqu Nullam arcu liquam here was consequat.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="events-list mb-3">
                @if(count($Blogs) != 0)
                    <div class="row">
                        @foreach($Blogs as $Blog)
                        <div class="col-xl-6 col-lg-6">
                            
                            <div class="single-events mb-4">
                                <div class="events-wrapper">
                                    <div class="events-inner d-flex">
                                        <div class="events-thumb">
                                            <img src="/UI/blog_img/{{$Blog->blog_img}}" alt="" style="
                                            width: 200px;
                                            height: 212px;
                                        ">
                                        </div>
                                        <div class="events-text white-bg">
                                            <div class="event-text-heading d-flex mb-3">
                                                <div class="events-calendar text-center f-left">
                                                    <span class="date">{{date('d', strtotime($Blog->date))}}</span>
                                                    <span class="month">{{date('M Y', strtotime($Blog->date))}}</span>
                                                </div>
                                                <div class="events-text-title clearfix">
                                                    <a href="/teachers/templates/blogs_details/{{$Blog->id}}" target="_blank">
                                                        <h4>{{$Blog->blog_title}}</h4>
                                                    </a>
                                                    <div class="time-area">
                                                        <span class="fas fa-clock"></span>
                                                        <span class="published-time">{{$Blog->time}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="events-para">
                                                <p>{!!Str::words($Blog->description, 10 ) !!}</p>
                                            </div>
                                            <div class="events-speaker">
                                                <h2>Speaker : <span>{{$Blog->speaker}}</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        <div class="col-xl-6 col-lg-6">
                            <div class="single-events mb-4">
                                <div class="events-wrapper">
                                    <div class="events-inner d-flex">
                                        <div class="events-thumb">
                                            <img src="{{URL::asset('UI/themes/images/events/eventsthumb1.png')}}" alt="" style="
                                            width: 200px;
                                            height: 212px;
                                        ">
                                        </div>
                                        <div class="events-text white-bg">
                                            <div class="event-text-heading d-flex mb-3">
                                                <div class="events-calendar text-center f-left">
                                                    <span class="date">25</span>
                                                    <span class="month">Sep, 2018</span>
                                                </div>
                                                <div class="events-text-title clearfix">
                                                    <a href="#">
                                                        <h4>Business Conferences</h4>
                                                    </a>
                                                    <div class="time-area">
                                                        <span class="fas fa-clock"></span>
                                                        <span class="published-time">05:23 AM - 09:23 AM</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="events-para">
                                                <p>Event is veries fermentum consequat mi fonec has fermentum ellentesque malesuada.</p>
                                            </div>
                                            <div class="events-speaker">
                                                <h2>Speaker : <span>Alexzender</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single-events mb-4">
                                <div class="events-wrapper">
                                    <div class="events-inner d-flex">
                                        <div class="events-thumb">
                                            <img src="{{URL::asset('UI/themes/images/events/eventsthumb2.png')}}" alt="" style="
                                            width: 200px;
                                            height: 212px;
                                        ">
                                        </div>
                                        <div class="events-text white-bg">
                                            <div class="event-text-heading d-flex mb-3">
                                                <div class="events-calendar text-center f-left">
                                                    <span class="date">25</span>
                                                    <span class="month">Sep, 2018</span>
                                                </div>
                                                <div class="events-text-title clearfix">
                                                    <a href="#">
                                                        <h4>Workshop Marketing</h4>
                                                    </a>
                                                    <div class="time-area">
                                                        <span class="fas fa-clock"></span>
                                                        <span class="published-time">05:23 AM - 09:23 AM</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="events-para">
                                                <p>Event is veries fermentum consequat mi fonec has fermentum ellentesque malesuada.</p>
                                            </div>
                                            <div class="events-speaker">
                                                <h2>Speaker : <span>Alexzender</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="single-events mb-4">
                                <div class="events-wrapper">
                                    <div class="events-inner d-flex">
                                        <div class="events-thumb">
                                            <img src="{{URL::asset('UI/themes/images/events/eventsthumb3.png')}}" alt="" style="
                                            width: 200px;
                                            height: 212px;
                                        ">
                                        </div>
                                        <div class="events-text white-bg">
                                            <div class="event-text-heading d-flex mb-3">
                                                <div class="events-calendar text-center f-left">
                                                    <span class="date">25</span>
                                                    <span class="month">Sep, 2018</span>
                                                </div>
                                                <div class="events-text-title clearfix">
                                                    <a href="#">
                                                        <h4>Admission Fair 2017</h4>
                                                    </a>
                                                    <div class="time-area">
                                                        <span class="fas fa-clock"></span>
                                                        <span class="published-time">05:23 AM - 09:23 AM</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="events-para">
                                                <p>Event is veries fermentum consequat mi fonec has fermentum ellentesque malesuada.</p>
                                            </div>
                                            <div class="events-speaker">
                                                <h2>Speaker : <span>Alexzender</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single-events mb-4">
                                <div class="events-wrapper">
                                    <div class="events-inner d-flex">
                                        <div class="events-thumb">
                                            <img src="{{URL::asset('UI/themes/images/events/eventsthumb4.png')}}" alt="" style="
                                            width: 200px;
                                            height: 212px;
                                        ">
                                        </div>
                                        <div class="events-text white-bg">
                                            <div class="event-text-heading d-flex mb-3">
                                                <div class="events-calendar text-center f-left">
                                                    <span class="date">25</span>
                                                    <span class="month">Sep, 2018</span>
                                                </div>
                                                <div class="events-text-title clearfix">
                                                    <a href="#">
                                                        <h4>Learning Spoken English</h4>
                                                    </a>
                                                    <div class="time-area">
                                                        <span class="fas fa-clock"></span>
                                                        <span class="published-time">05:23 AM - 09:23 AM</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="events-para">
                                                <p>Event is veries fermentum consequat mi fonec has fermentum ellentesque malesuada.</p>
                                            </div>
                                            <div class="events-speaker">
                                                <h2>Speaker : <span>Alexzender</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
            </div>
            <div class="events-view-btn">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="view-all-events text-center">
                            <a target="_blank" href="/teachers/templates/blogs_list/{{$Users->slug}}" class="pink-btn">view all events<span>→</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<section class="popular_courses">
    <div class="container"> 
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sub_title">
                    <h2>Sample Videos</h2>
                    <p class="pink-color">Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra.</p>  
                </div><!-- ends: .section-header -->
            </div>
            @if(isset($YoutubeLinks))
                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <iframe width="350" height="250" src="{{$YoutubeLinks->yt_link1}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div><!-- Ends: . -->



                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <iframe width="350" height="250" src="{{$YoutubeLinks->yt_link2}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div><!-- Ends: . -->

                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <iframe width="350" height="250" src="{{$YoutubeLinks->yt_link3}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div><!-- Ends: . -->    
            @else
                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="single-courses">
                        <div class="courses_banner_wrapper">
                            <div class="courses_banner"><a href="#"><img src="{{URL::asset('UI/themes/images/courses/courses_1.jpg')}}" alt="" class="img-fluid"></a></div>
                            <div class="purchase_price">
                                <a href="#" class="read_more-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </div>
                    </div><!-- Ends: .single courses -->
                </div><!-- Ends: . -->



                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="single-courses">
                        <div class="courses_banner_wrapper">
                            <div class="courses_banner"><a href="#"><img src="{{URL::asset('UI/themes/images/courses/courses_2.jpg')}}" alt="" class="img-fluid"></a></div>
                            <div class="purchase_price">
                                <a href="#" class="read_more-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </div>
                    </div><!-- Ends: .single courses -->
                </div><!-- Ends: . -->

                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="single-courses">
                        <div class="courses_banner_wrapper">
                            <div class="courses_banner"><a href="#"><img src="{{URL::asset('UI/themes/images/courses/courses_3.jpg')}}" alt="" class="img-fluid"></a><!--<iframe width="384" height="238" src="https://www.youtube.com/embed/FjHGZj2IjBk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--></div>
                            <div class="purchase_price">
                                <a href="#" class="read_more-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </div>
                    </div><!-- Ends: .single courses -->
                </div><!-- Ends: . -->    
            @endif
                                               
        </div>

    </div>
</section><!-- ./ End Courses Area section -->



<!-- Footer -->  
<footer class="footer_2 bgfooter">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                 <div class="col-12 col-md-12 col-lg-12">
                    <div class="copyright">Copyright &copy;2020 All rights reserved | Design is made with by 
                        <a target="_blank" href="https://www.techitalents.com">Techitalents</a>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="shapes_bg">
        <img src="images/shapes/footer_2.png" alt="" class="shape_1">
    </div>    
</footer><!-- End Footer -->

<section id="scroll-top" class="scroll-top">
    <h2 class="disabled">Scroll to top</h2>
    <div class="to-top pos-rtive">
        <a href="#"><i class = "flaticon-right-arrow"></i></a>
    </div>
</section>

    <!-- JavaScript -->
    <script src="{{URL::asset('UI/themes/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/popper.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/bootstrap.min.js')}}"></script>
    <!-- Revolution Slider -->
    <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.tools.min.js')}}"></script> 
    <script src="{{URL::asset('UI/themes/js/jquery.magnific-popup.min.js')}}"></script>     
    <script src="{{URL::asset('UI/themes/js/owl.carousel.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/slick.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/jquery.meanmenu.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/wow.min.js')}}"></script> 
    <!-- Counter Script -->
    <script src="{{URL::asset('UI/themes/js/waypoints.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/jquery.counterup.min.js')}}"></script>

    <!-- Revolution Extensions -->
    <script src="{{URL::asset('UI/themes/js/custom.js')}}"></script>  
    
</body>
</html>
