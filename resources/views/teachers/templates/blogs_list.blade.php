<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Ecology Theme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Education - Welcome to Teacher Page</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/bootstrap.min.css')}}">
    <!-- Font awsome CSS -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/font-awesome.min.css')}}">    
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/magnific-popup.css')}}">    
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/owl.theme.css')}}">     
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/animate.css')}}"> 
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/slick.css')}}">  
   
    <!-- Mean Menu-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/assets/meanmenu.css')}}">
    <!-- main style-->
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/responsive.css')}}">
    <link rel="stylesheet" href="{{URL::asset('UI/themes/css/demo.css')}}">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="header_tow header_inner blog_page">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>    
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="info_wrapper">
                        <div class="contact_info">                   
        					                  
                        </div>
                        <div class="login_info">
                             {{-- <ul class="d-flex">                                
                                <li class="nav-item"><a href="#" class="nav-link join_now js-modal-show"><i class="flaticon-padlock"></i>Log In</a></li>
                            </ul> --}}
                            <a href="/teachers/templates/contact/{{$Users->slug}}" title="" class="apply_btn">Book a Demo Class</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="edu_nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-faded">
                {{-- <a class="navbar-brand" href="#"><img src="images/logo2.png" alt="logo"></a> --}}
                <a class="navbar-brand" href="#">Logo</a>
                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav nav lavalamp ml-auto menu">
                        {{-- <li class="nav-item"><a href="#" class="nav-link active">Home</a>
                        </li>              --}}

                    </ul>
                </div>
            </nav><!-- END NAVBAR -->
        </div> 
    </div>

    <div class="intro_wrapper">
        <div class="container">  
            <div class="row">        
                 <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="intro_text">
                        <h1>Blog List</h1>
                        <div class="pages_links">
                            <a href="#" title="">Home</a>
                            <a href="#" title="" class="active">Blog List</a>
                        </div>
                    </div>
                </div>              

            </div>
        </div> 
    </div> 
</header><!--  End header section-->




<section class="login_signup_option">
    <div class="l-modal is-hidden--off-flow js-modal-shopify">
        <div class="l-modal__shadow js-modal-hide"></div>
        <div class="login_popup login_modal_body">
            <div class="Popup_title d-flex justify-content-between">
                <h2 class="hidden">&nbsp;</h2>
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12 col-lg-12 login_option_btn">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#login" role="tab">Login</a></li>
                            <!--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#panel2" role="tab">Register</a></li>-->
                        </ul>
                    </div>
                    <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                        <!-- Tab panels -->
                        <div class="tab-content card">
                            <!--Login-->
                            <div class="tab-pane fade in show active" id="login" role="tabpanel">
                                <form action="#">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="email" class="form-control" placeholder="Username">
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12 d-flex justify-content-between login_option">
                                            <a href="#" title="" class="forget_pass">Forget Password ?</a>
                                            <button type="submit" class="btn btn-default login_btn">Login</button>
                                        </div> 
                                    </div>
                                </form>
                            </div>
                            <!--/.Panel 1-->
                            <!--Panel 2-->
                            <div class="tab-pane fade" id="panel2" role="tabpanel">
                                <form action="#" class="register">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Name</label>
                                                <input type="text" class="form-control" placeholder="Username">
                                            </div>
                                        </div>                                        
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Email</label>
                                                <input type="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label  class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-12 col-md-12 col-lg-12 d-flex justify-content-between login_option">
                                            <button type="submit" class="btn btn-default login_btn">Register</button>
                                        </div> 
                                    </div>
                                </form>
                            </div><!--/.Panel 2-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  <!-- End Login Signup Option -->


<section class="latest_news_2" id="latest_news_style_2">
    <div class="container">
        <div class="row">

            @foreach($Blogs as $Blog)
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                 <div class="single_item">
                    <div class="item_wrapper">
                        <div class="blog-img">
                            <a href="#" title=""><img src="/UI/blog_img/{{$Blog->blog_img}}" alt="" class="img-fluid" style="
                                width: 421px;
                                height: 262px;
                            "></a>
                        </div>
                        <h3><a href="/teachers/templates/blogs_details/{{$Blog->id}}" title="">{{$Blog->blog_title}}</a></h3> 
                    </div>
                    <div class="blog_title">
                        <ul class="post_bloger">
                            <li><i class="fas fa-user"></i>{{$Users->name}}</li> 
                            {{-- <li><i class="fas fa-comment"></i>0 Comments</li>
                            <li><i class="fas fa-thumbs-up"></i> 0 Like</li> --}}
                        </ul>               
                    </div> 
                </div>
            </div>            
            @endforeach
           

            {{-- <div class="pagination_blog">
                <ul>
                    <li><a href="#">1</a></li>
                    <li class="current"><a href="#">2</a></li>
                    <li><a href="#" class=""><i class='flaticon-right-arrow'></i></a></li>
                </ul>
            </div>  --}}
        </div>
    </div>
</section><!-- End Our Blog -->


<!-- Footer -->  
<footer class="footer_2 bgfooter">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                 <div class="col-12 col-md-12 col-lg-12">
                    <div class="copyright">Copyright &copy;2020 All rights reserved | Design is made with by 
                        <a target="_blank" href="https://www.techitalents.com">Techitalents</a>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="shapes_bg">
        <img src="images/shapes/footer_2.png" alt="" class="shape_1">
    </div>    
</footer><!-- End Footer -->

<section id="scroll-top" class="scroll-top">
    <h2 class="disabled">Scroll to top</h2>
    <div class="to-top pos-rtive">
        <a href="#"><i class = "flaticon-right-arrow"></i></a>
    </div>
</section>

    <!-- JavaScript -->
    <script src="{{URL::asset('UI/themes/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/popper.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/bootstrap.min.js')}}"></script>
    <!-- Revolution Slider -->
    <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/assets/revolution/jquery.themepunch.tools.min.js')}}"></script> 
    <script src="{{URL::asset('UI/themes/js/jquery.magnific-popup.min.js')}}"></script>     
    <script src="{{URL::asset('UI/themes/js/owl.carousel.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/slick.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/jquery.meanmenu.min.js')}}"></script>   
    <script src="{{URL::asset('UI/themes/js/wow.min.js')}}"></script> 
    <!-- Counter Script -->
    <script src="{{URL::asset('UI/themes/js/waypoints.min.js')}}"></script>
    <script src="{{URL::asset('UI/themes/js/jquery.counterup.min.js')}}"></script>

    <!-- Revolution Extensions -->
    <script src="{{URL::asset('UI/themes/js/custom.js')}}"></script>  
    
</body>
</html>
