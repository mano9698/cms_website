@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit My Banner</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Updating My Banner Images</strong></div>
              <div class="block-body">
                @if(session('message'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif
                <form class="form-horizontal" action="store_or_update_banners" method="post" enctype="multipart/form-data">
                    @csrf
                <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Upload Banner Image</label>
                    <div class="col-sm-6">
                      <table id="item-add" style="width:100%;">
                  <tr class="list-item">
                    <td>
                      <div class="row">
                        <div class="col-md-9">
                          <label class="col-form-label">Banner Image</label>
                          <div>
                            <input class="form-control" name="banner1" type="file" value="">
                          </div>
                        </div>
                        {{-- <div class="col-md-1">
                          <label class="col-form-label">Close</label>
                          <div class="form-group">
                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                          </div>
                        </div> --}}
                      </div>
                    </td>
                  </tr>

                  <tr class="list-item">
                    <td>
                      <div class="row">
                        <div class="col-md-9">
                          <label class="col-form-label">Banner Image</label>
                          <div>
                            <input class="form-control" name="banner2" type="file" value="">
                          </div>
                        </div>
                        {{-- <div class="col-md-1">
                          <label class="col-form-label">Close</label>
                          <div class="form-group">
                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                          </div>
                        </div> --}}
                      </div>
                    </td>
                  </tr>

                  <tr class="list-item">
                    <td>
                      <div class="row">
                        <div class="col-md-9">
                          <label class="col-form-label">Banner Image</label>
                          <div>
                            <input class="form-control" name="banner3" type="file" value="">
                          </div>
                        </div>
                        {{-- <div class="col-md-1">
                          <label class="col-form-label">Close</label>
                          <div class="form-group">
                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                          </div>
                        </div> --}}
                      </div>
                    </td>
                  </tr>
                </table>
                    </div>
                    {{-- <div class="col-sm-2">
<div class="form-group"><button type="button" class="btn button-sm purple add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add More Images</button> </div>
                    </div> --}}
                  </div>
                 
                  <div class="line"></div>      
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection