@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Adding Blogs On Website</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Adding New Blogs</strong></div>
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                
                <form class="form-horizontal" action="/teachers/store_blogs" method="POST" enctype="multipart/form-data">
                    @csrf                    
                   <!--<div class="form-group row mb-0">
                    <div class="col-sm-12">
                      <table id="item-add" style="width:100%;">
                  <tr class="list-item">
                    <td>
                      <div class="row">
                        <div class="col-md-3">
                          <label class="col-form-label">Blog Name</label>
                          <div>
                            <input class="form-control" type="text" value="">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label class="col-form-label">Blog Image</label>
                          <div>
                            <input class="form-control" type="file" value="">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <label class="col-form-label">Date</label>
                          <div>
                            <input class="form-control" type="date" value="">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <label class="col-form-label">Time</label>
                          <div>
                            <input class="form-control" type="time" value="">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label class="col-form-label">Description</label>
                          <div>
                            <input class="form-control" type="text" value="">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <label class="col-form-label">Speaker</label>
                          <div>
                            <input class="form-control" type="text" value="">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <label class="col-form-label">Close</label>
                          <div class="form-group">
                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
                    </div>
                    
                   </div>
                   <div class="col-sm-2 pl-0">
                  
                      <div class="form-group"><button type="button" class="btn button-sm purple add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add More Blogs</button> </div>
                    </div>-->
                    <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Blog Title</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="blog_title" placeholder="">
                    </div>
                  </div>
                  <div class="line"></div> 
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Blog Image</label>
                    <div class="col-sm-6">
                      <input type="file" class="form-control" id="blog-img" name="blog_img" placeholder="">

                      <div id="" class="image-previewer" data-cropzee="blog-img"></div>
                    </div>
                  </div>
                  <div class="line"></div>   
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Date</label>
                    <div class="col-sm-6">
                      <input type="date" class="form-control" name="date" placeholder="">
                    </div>
                  </div>
                  <div class="line"></div> 
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Time</label>
                    <div class="col-sm-6">
                      <input type="time" class="form-control" name="time" placeholder="">
                    </div>
                  </div>
                  <div class="line"></div> 
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Blog Description</label>
                    <div class="col-sm-6">
                      <textarea type="text" class="form-control ckeditor" name="description" placeholder=""></textarea>
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Speaker</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="speaker" placeholder="">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Add New</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
  <script>
    $(document).ready(function(){
      $("#blog-img").cropzee({startSize: [85, 85, '%'],});

    });
  </script>
  @endsection