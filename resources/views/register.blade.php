<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Teacher Profile Admin </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{URL::asset('UI/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{URL::asset('UI/vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="{{URL::asset('UI/css/font.css')}}">
    <!-- Google fonts - Muli-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{URL::asset('UI/css/style.default.css')}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{URL::asset('UI/css/custom.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>Teacher Profile</h1>
                  </div>
                  <p>Dashboard</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                  <form class="text-left form-validate" action="/add_users" method="post">
                    @csrf

                    {{-- <div class="form-group-material">
                      <input id="register-firstname" type="text" name="registerFirstname" required data-msg="Please enter your First name" class="input-material">
                      <label for="register-firstname" class="label-material">First Name</label>
                    </div> --}}
					  <div class="form-group-material">
                      <input id="register-lastname" type="text" name="name" required data-msg="Please enter your Website name" class="input-material">
                      <label for="register-lastname" class="label-material">Website Name</label>
                    </div>
                    <div class="form-group-material">
                      <input id="register-email" type="email" name="email" required data-msg="Please enter a valid email address" class="input-material">
                      <label for="register-email" class="label-material">Email Address</label>
                    </div>
					  <div class="form-group-material">
                      <input id="register-contact" type="text" name="contact" required data-msg="Please enter your Contact No" class="input-material">
                      <label for="register-contact" class="label-material">Contact No</label>
                    </div>
					  <div class="form-group-material">
                      <input id="register-country" type="text" name="country" required data-msg="Please enter your Country" class="input-material">
                      <label for="register-country" class="label-material">Country</label>
                    </div>
					  <div class="form-group-material">
                      <input id="register-state" type="text" name="state" required data-msg="Please enter your State" class="input-material">
                      <label for="register-state" class="label-material">State</label>
                    </div>
					  <div class="form-group-material">
                      <input id="register-city" type="text" name="city" required data-msg="Please enter your City" class="input-material">
                      <label for="register-city" class="label-material">City</label>
                    </div>
					  <div class="form-group-material">
                      <input id="register-pincode" type="text" name="pincode" required data-msg="Please enter your Pincode" class="input-material">
                      <label for="register-pincode" class="label-material">Pincode</label>
                    </div>
					  <div class="form-group-material">
                      <input id="register-area" type="text" name="area" required data-msg="Please enter your Area" class="input-material">
                      <label for="register-area" class="label-material">Area</label>
                    </div>
                    <div class="form-group-material">
                      <input id="register-password" type="password" name="password" required data-msg="Please enter your password" class="input-material">
                      <label for="register-password" class="label-material">Password        </label>
                    </div>

                    <div class="form-group terms-conditions">
                      <input id="register-agree" name="registerAgree" type="checkbox" required value="1" data-msg="Your agreement is required" class="checkbox-template">
                      <label for="register-agree">I agree with the terms and policy</label>
                    </div>
                    <div class="form-group">
                      <input id="register" type="submit" value="Register" class="btn btn-primary">
                    </div>
                  </form><small>Already have an account? </small><a href="/" class="signup">Login</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
         <p>2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="{{URL::asset('UI/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('UI/vendor/popper.js/umd/popper.min.js')}}"> </script>
    <script src="{{URL::asset('UI/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('UI/vendor/jquery.cookie/jquery.cookie.js')}}"> </script>
    <script src="{{URL::asset('UI/vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{URL::asset('UI/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{URL::asset('UI/js/front.js')}}"></script>
  </body>
</html>