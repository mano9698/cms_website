<!DOCTYPE html>
<html>
  <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>::Welcome to Teacher Profile::</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{URL::asset('UI/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{URL::asset('UI/vendor/font-awesome/css/font-awesome.min.css')}}">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="{{URL::asset('UI/css/font.css')}}">
    <!-- Google fonts - Muli-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{URL::asset('UI/css/style.default.css')}}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{URL::asset('UI/css/custom.css')}}">

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{URL::asset('UI/css/button.css')}}"> --}}
    <!-- Favicon-->
    <link rel="shortcut icon" href="../img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    @include('common.header')
    <div class="d-flex align-items-stretch">
      <!-- Sidebar Navigation-->
      @include('common.sidebar')
      <!-- Sidebar Navigation end-->
      @yield('Content')
    </div>
    <!-- JavaScript files-->
    <script src="{{URL::asset('UI/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('UI/vendor/popper.js/umd/popper.min.js')}}"> </script>
    <script src="{{URL::asset('UI/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('UI/vendor/jquery.cookie/jquery.cookie.js')}}"> </script>
    <script src="{{URL::asset('UI/vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{URL::asset('UI/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{URL::asset('UI/js/front.js')}}"></script>

    <script src="{{URL::asset('UI/js/cropzee.js')}}"></script>

    <script>
      $(document).ready(function(){
        $("#cropzee-input").cropzee({startSize: [85, 85, '%'],});
      });
    </script>



    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
           $('.ckeditor').ckeditor();
        });
    </script>
    
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    @yield('JSScript')
  </body>
</html>