@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">List of Inquiries On Website</h2>
      </div>
    </div>

<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">

              <div class="table-responsive">
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email Id</th>
                      <th>Contact No</th>
                      <th>WhatsappNo</th>
                      <th>Country/State/City</th>
                      <th>Description</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($ContactLeadsModel as $Inquiry)
                    <tr>
                        <td>{{$Inquiry->name}}</td>
                      <td>{{$Inquiry->email}}</td>
                      <td>{{$Inquiry->contact}}</td>
                      <td>{{$Inquiry->whatsapp_no}}</td>
                      <td>{{$Inquiry->country}}/{{$Inquiry->city_state}}</td>
                      <td>{{$Inquiry->requirement}}</td>
                      <td>
                        {{date('d M Y', strtotime($Inquiry->created_at))}}
                     </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>



        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">

           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection
