@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Teachers list</h2>
      </div>
    </div>
    
    <!-- Breadcrumb-->
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Teachers List            </li>
      </ul>
    </div>
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="block margin-bottom-sm">
              
              <div class="table-responsive"> 
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Name & Lastname</th>
                      <th>Email Id</th>
                      <th>Contact Number</th>
                      <th>Country/State/City</th>
                      {{-- <th>Area - Pincode</th> --}}
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($Users as $User)
                    <tr>
                      <th scope="row">{{$User->id}}</th>
                      <!--<td><div class="avatar"> <img src="../img/avatar-1.jpg" alt="..." class="img-fluid"></div><a href="#" class="name"></td>-->
                      <td><strong class="d-block">{{$User->name}}</strong></td>
                      <td>{{$User->email}}</td>
                      <td>{{$User->contact}}</td>
                      <td>{{$User->country}}/{{$User->state}}/{{$User->city}}</td>
                      {{-- <td>HSR Layout - 560087</td> --}}
                      <td>
                        <input data-id="{{$User->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $User->status ? 'checked' : '' }}>

                        <a href="/admin/edit_teachers/{{$User->id}}" target="_blank" class="btn button-sm blue">Edit</a>
                        <?php 
                          $CHeckTemplateSelection = DB::table('template_selection')
                                                    ->where('user_id', $User->id)
                                                    ->first();
                        ?>
                        @if($CHeckTemplateSelection)
                        <a href="/sites/{{$User->slug}}" target="_blank" class="btn button-sm orange" title="view website">Website</a>
                        @else

                        @endif
                     </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          
          
          
        </div>
      </div>
    </section>
    

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teachers Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection


  @section('JSScript')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  


    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
           console.log(status);
          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/admin/change_status',
              data: {'status': status, 'id': id},
              success: function(data){
                console.log(data.success)
                alert(data.success);
                
              }
          });
      })
    })
  </script>
@endsection