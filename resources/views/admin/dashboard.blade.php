@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Dashboard</h2>
      </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="statistic-block block">
              <div class="progress-details d-flex align-items-end justify-content-between">
                <div class="title">
                  <div class="icon"><i class="icon-user-1"></i></div><strong>New Teachers</strong>
                </div>
                <div class="number dashtext-3">27</div>
              </div>
              <div class="progress progress-template">
                <div role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-3"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    
    <section class="no-padding-bottom">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="checklist-block block">
              <div class="title"><strong>To Do List</strong></div>
              <div class="checklist">
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-1" name="input-1" class="checkbox-template">
                  <label for="input-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-2" name="input-2" checked class="checkbox-template">
                  <label for="input-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-3" name="input-3" class="checkbox-template">
                  <label for="input-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-4" name="input-4" class="checkbox-template">
                  <label for="input-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-5" name="input-5" class="checkbox-template">
                  <label for="input-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
                <div class="item d-flex align-items-center">
                  <input type="checkbox" id="input-6" name="input-6" class="checkbox-template">
                  <label for="input-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">                                           
            <div class="messages-block block">
              <div class="title"><strong>New Messages</strong></div>
              <div class="messages"><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="../img/avatar-3.jpg" alt="..." class="img-fluid">
                    <div class="status online"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Nadia Halsey</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">9:30am</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="../img/avatar-2.jpg" alt="..." class="img-fluid">
                    <div class="status away"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Peter Ramsy</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">7:40am</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="../img/avatar-1.jpg" alt="..." class="img-fluid">
                    <div class="status busy"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Sam Kaheil</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">6:55am</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="../img/avatar-5.jpg" alt="..." class="img-fluid">
                    <div class="status offline"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Sara Wood</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">10:30pm</small></div></a><a href="#" class="message d-flex align-items-center">
                  <div class="profile"><img src="../img/avatar-1.jpg" alt="..." class="img-fluid">
                    <div class="status online"></div>
                  </div>
                  <div class="content">   <strong class="d-block">Nader Magdy</strong><span class="d-block">lorem ipsum dolor sit amit</span><small class="date d-block">9:47pm</small></div></a></div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teachers Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection