@extends('base')
@section('Content')
<div class="page-content">
    <div class="page-header">
      <div class="container-fluid">
        <h2 class="h5 no-margin-bottom">Edit My Details On Website</h2>
      </div>
    </div>
    
<section class="no-padding-top">
      <div class="container-fluid">
        <div class="row">

          <!-- Form Elements -->
          <div class="col-lg-12">
            <div class="block">
              <div class="title"><strong>Updating My Details</strong></div>
              <div class="block-body">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                
                <form class="form-horizontal" action="/teachers/update_profile" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">First Name</label>
                    <div class="col-sm-6">
                        <input type="hidden" class="form-control" name="id" placeholder="Suresh" value="{{$Users->id}}">

                        <input type="text" class="form-control" placeholder="Suresh" value="{{$Users->name}}">
                    </div>
                  </div>                       
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Email Address</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" placeholder="info@techitalents.com" value="{{$Users->email}}">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Upload Profile Image</label>
                    <div class="col-sm-6">
                      <input type="file" class="form-control" value="" name="profile_pic">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Contact number</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" name="contact" value="{{$Users->contact}}">
                    </div>
                  </div>
                  <div class="line"></div>
          <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Address</label>
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-md-4">
                          <input type="text" placeholder="Country" name="country" class="form-control" value="{{$Users->country}}">
                        </div>
                        <div class="col-md-4">
                          <input type="text" placeholder="State" name="state"  class="form-control" value="{{$Users->state}}">
                        </div>
                        <div class="col-md-4">
                          <input type="text" placeholder="City" name="city" class="form-control" value="{{$Users->city}}">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Qualification</label>
                    <div class="col-sm-6">
                      <input type="text" value="{{$Users->qualification}}" name="qualification"  class="form-control">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Experience</label>
                    <div class="col-sm-6">
                      <input type="text" value="{{$Users->exp}}" class="form-control" name="exp">
                    </div>
                  </div>
                  <div class="line"></div>
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">About Me</label>
                    <div class="col-sm-6">
                      <textarea type="text" value="{{$Users->about_me}}" class="form-control" name="about_me">{{$Users->about_me}}</textarea>
                    </div>
                  </div>
                   
                  <div class="line"></div>      
                  <div class="form-group row">
                    <div class="col-sm-9">
                      <button type="submit" class="btn btn-secondary">Cancel</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer">
      <div class="footer__block block no-margin-bottom">
        <div class="container-fluid text-center">
          
           <p class="no-margin-bottom">2020 &copy; Teacher Profile. Designed By <a target="_blank" href="https://www.techitalents.com">Techitalents</a>.</p>
        </div>
      </div>
    </footer>
  </div>
  @endsection