<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'AuthendicationController@login');

Route::get('/register', 'AuthendicationController@register');

Route::post('/add_users', 'AuthendicationController@add_users');

Route::post('/login', 'AuthendicationController@admin_login');

Route::get('/teacher_logout', 'AuthendicationController@teacher_logout');

Route::get('/admin_logout', 'AuthendicationController@admin_logout');





Route::group(['prefix' => '/admin'], function () {

    Route::get('/dashboard', 'Admin\HomeController@dashboard');

    Route::get('/add_teachers', 'Admin\HomeController@add_teachers');

    Route::get('/edit_teachers/{id}', 'Admin\HomeController@edit_teachers');

    Route::get('/teachers_list', 'Admin\HomeController@teachers_list');

    Route::get('/inquiries_list', 'Admin\HomeController@inquiries_list');

    Route::get('/change_password', 'Admin\HomeController@change_password');

    Route::post('/store_teachers', 'Admin\HomeController@store_teachers');

    Route::post('/update_password', 'Admin\HomeController@update_password');

    Route::post('/change_status', 'Admin\HomeController@changeStatus');

    Route::get('/contact_leads_list', 'Admin\HomeController@contact_leads_list');

    // Route::post('/changeStatus', 'UI\AdminController@ChangeStatus');

    // Route::get('/students_list', 'UI\AdminController@students_list');

    // // Route::post('/delete_fees', 'UI\UsersController@delete_fees');

    // // Route::post('/delete_tuition', 'UI\UsersController@delete_tuition');

    // // Route::post('/delete_info', 'UI\UsersController@delete_info');


    // Route::get('/add_teachers/{id}', 'UI\AdminController@add_teachers');

    // Route::post('/store_teachers_list', 'UI\AdminController@store_teachers_list');

    // Route::post('/delete_teachers_list', 'UI\AdminController@delete_teachers_list');
});



Route::group(['prefix' => '/teachers'], function () {

    Route::get('/dashboard', 'Teachers\HomeController@dashboard');

    Route::get('/banner', 'Teachers\HomeController@banner');

    Route::get('/announcement', 'Teachers\HomeController@announcement');

    Route::post('/store_or_update_banners', 'Teachers\HomeController@store_or_update_banners');

    Route::post('/add_or_update_announcement', 'Teachers\HomeController@add_or_update_announcement');

    Route::get('/personal_details', 'Teachers\HomeController@personal_details');

    Route::get('/tuition', 'Teachers\HomeController@tuition');

    Route::post('/update_profile', 'Teachers\HomeController@update_profile');

    Route::post('/store_or_update_tuitions', 'Teachers\HomeController@store_or_update_tuitions');

    Route::get('/blogs_list', 'Teachers\HomeController@blogs_list');

    Route::get('/add_blogs', 'Teachers\HomeController@add_blogs');

    Route::get('/edit_blogs/{id}', 'Teachers\HomeController@edit_blogs');

    Route::post('/store_blogs', 'Teachers\HomeController@store_blogs');

    Route::post('/update_blogs', 'Teachers\HomeController@update_blogs');

    Route::get('/youtube_links', 'Teachers\HomeController@youtube_links');

    Route::post('/add_or_update_yt_links', 'Teachers\HomeController@add_or_update_yt_links');


    Route::get('/templates', 'Teachers\HomeController@templates');

    Route::get('/inquiries_list', 'Teachers\HomeController@inquiries_list');

    Route::get('/change_password', 'Teachers\HomeController@change_password');

    Route::post('/update_password', 'Teachers\HomeController@update_password');

});


Route::group(['prefix' => '/teachers/templates'], function () {

    Route::get('/{id}/preview', 'Teachers\TemplatesController@template_preview');

    // Route::get('/template1', 'Teachers\TemplatesController@template1');

    Route::get('/contact/{name}', 'Teachers\TemplatesController@contact');

    Route::post('/add_contact', 'Teachers\TemplatesController@add_contact');

    Route::post('/add_template_selection', 'Teachers\TemplatesController@add_template_selection');

    Route::get('/blogs_list/{name}', 'Teachers\TemplatesController@blogs_list');

    Route::get('/blogs_details/{id}', 'Teachers\TemplatesController@blogs_details');

});


Route::get('/sites/{name}', 'Teachers\TemplatesController@templates');
