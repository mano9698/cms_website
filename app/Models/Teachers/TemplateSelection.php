<?php

namespace App\Models\Teachers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateSelection extends Model
{
    use HasFactory;

    protected $table = 'template_selection';

    protected $fillable = ['user_id','template_id'];
}
