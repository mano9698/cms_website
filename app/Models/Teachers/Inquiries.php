<?php

namespace App\Models\Teachers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquiries extends Model
{
    use HasFactory;

    protected $table = 'inquiries';

    protected $fillable = ['user_id','name', 'email', 'contact', 'whatsapp_no', 'country', 'state_city', 'requirements'];
}
