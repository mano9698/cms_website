<?php

namespace App\Models\Teachers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YoutubeLinks extends Model
{
    use HasFactory;

    protected $table = 'youtube_links';

    protected $fillable = ['user_id','yt_link1', 'yt_link2', 'yt_link3'];
}
