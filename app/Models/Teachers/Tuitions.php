<?php

namespace App\Models\Teachers;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tuitions extends Model
{
    use HasFactory;

    protected $table = 'tuition';

    protected $fillable = ['user_id','benefits_1', 'benefits_2', 'benefits_3', 'tuition_img1', 'tuition_img2', 'tuition_img2'];
}
