<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = ['name','slug', 'email', 'password', 'profile_pic', 'contact', 'address', 'qualification', 'exp', 'about_me', 'announcement'];
}
