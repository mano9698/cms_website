<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Teachers\Tuitions;
use App\Models\Teachers\Blogs;
use App\Models\Teachers\YoutubeLinks;
use App\Models\Teachers\Inquiries;
use App\Models\ContactLeadsModel;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class HomeController extends Controller
{
    public function dashboard(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        // $Users = Users::where('user_type', 2)
        //                     ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('admin.dashboard');
    }

    public function add_teachers(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        // $Users = Users::where('user_type', 2)
        //                     ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('admin.add_teachers');
    }

    public function teachers_list(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        $Users = Users::where('user_type', 2)
                            ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('admin.teachers_list', compact('Users'));
    }

    public function edit_teachers($id){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        $Users = Users::where('id', $id)
                            ->first();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('admin.edit_teachers', compact('Users'));
    }

    public function inquiries_list(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $Inquiries = Inquiries::select('users.name as TeacherName', 'inquiries.*')->join('users', 'users.id', '=', 'inquiries.user_id')->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('admin.inquiries', compact('Inquiries'));
    }

    public function contact_leads_list(){
        $title = "Contact Leads";
        $ContactLeadsModel = ContactLeadsModel::get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('admin.contact_leads', compact('ContactLeadsModel'));
    }

    public function change_password(){
        $title ="Change Password";
        $UserId = Session::get('AdminId');
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('admin.change_password', compact('title'));
    }


    public function update_password(Request $request)
    {

        //  $this->validate($request, [

        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);

        $UserId = Session::get('AdminId');

       $hashedPassword = Auth::guard('super_admin')->user()->password;

    //    if (\Hash::check($request->oldpassword , $hashedPassword )) {

         if (!\Hash::check($request->newpassword , $hashedPassword)) {

              $users =Users::find($UserId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $UserId)->update( array( 'password' =>  $users->password));

              session()->flash('message','password updated successfully');
              return redirect()->back();
            }

            else{
                  session()->flash('message','new password can not be the old password!');
                  return redirect()->back();
                }

        //    }

        //   else{
        //        session()->flash('message','old password doesnt matched ');
        //        return redirect()->back();
        //      }

       }

    public function store_teachers(Request $request){
        // $UserId = Session::get('TeacherId');

        $Users = new Users();

        $Users->name = $request->name;
        $Users->slug = strtolower($request->name);
        $Users->email = $request->email;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;

        if($request->hasfile('profile_pic')){
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $dir = 'UI/profile_pic/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('profile_pic')->move($dir, $filename);

            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }


        $Users->contact = $request->contact;
        $Users->qualification = $request->qualification;
        $Users->exp = $request->exp;
        $Users->about_me = $request->about_me;
        $Users->password = Hash::make($request->password);
        $Users->status = 0;
        $Users->user_type = 2;


        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Profile Updated Successfully');
    }


    public function update_teachers(Request $request){
        // $UserId = Session::get('TeacherId');
        $id = $request->id;

        $Users = Users::where('id', $id)->first();

        $Users->name = $request->name;
        $Users->slug = strtolower($request->name);
        $Users->email = $request->email;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;

        if($request->hasfile('profile_pic')){
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $dir = 'UI/profile_pic/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('profile_pic')->move($dir, $filename);

            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }


        $Users->contact = $request->contact;
        $Users->qualification = $request->qualification;
        $Users->exp = $request->exp;
        $Users->about_me = $request->about_me;
        $Users->password = Hash::make($request->password);
        $Users->status = 0;
        $Users->user_type = 2;


        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Profile Updated Successfully');
    }

    public function changeStatus(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->status = $request->status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
}
