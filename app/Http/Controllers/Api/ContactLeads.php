<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactLeadsModel;

class ContactLeads extends Controller
{
    public function store_contact_leads(Request $request)
    {
        $ContactLeadsModel = new ContactLeadsModel();

        $ContactLeadsModel->name = $request->name;
        $ContactLeadsModel->email = $request->email;
        $ContactLeadsModel->contact = $request->contact;
        $ContactLeadsModel->whatsapp_no = $request->whatsapp_no;
        $ContactLeadsModel->country = $request->country;
        $ContactLeadsModel->city_state = $request->city;
        $ContactLeadsModel->requirement = $request->requirement;

        $SaveContactLeadsModel = $ContactLeadsModel->save();

        if($SaveContactLeadsModel){
            return response()->json(array(
                "error"=>False,
                "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }
}
