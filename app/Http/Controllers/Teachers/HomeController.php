<?php

namespace App\Http\Controllers\Teachers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teachers\Banners;
use App\Models\Users;
use App\Models\Teachers\Tuitions;
use App\Models\Teachers\Blogs;
use App\Models\Teachers\YoutubeLinks;
use App\Models\Teachers\Inquiries;
use App\Models\Teachers\TemplateSelection;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class HomeController extends Controller
{
    public function dashboard(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        // $Courses = Courses::where('teacher_id', $UserId)
        //                     ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.dashboard');
    }

    public function banner(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        // $Courses = Courses::where('teacher_id', $UserId)
        //                     ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.banner');
    }

    public function announcement(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        // $Courses = Courses::where('teacher_id', $UserId)
        //                     ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.announcement');
    }

    public function personal_details(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $Users = Users::where('id', $UserId)
                            ->first();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.personal_details', compact('Users'));
    }

    public function tuition(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $Tuitions = Tuitions::where('user_id', $UserId)
                            ->first();
        // echo json_encode($Tuitions);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.tuition', compact('Tuitions'));
    }

    public function blogs_list(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $Blogs = Blogs::where('user_id', $UserId)
                            ->get();
        // echo json_encode($Tuitions);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.blogs.blogs_list', compact('Blogs'));
    }

    public function add_blogs(){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        // $Blogs = Blogs::where('user_id', $UserId)
        //                     ->get();
        // echo json_encode($Tuitions);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.blogs.add_blogs');
    }

    public function edit_blogs($id){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');
        $Blogs = Blogs::where('id', $id)
                            ->first();
        // echo json_encode($Tuitions);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.blogs.edit_blogs', compact('Blogs'));
    }

    public function youtube_links(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $YoutubeLinks = YoutubeLinks::where('user_id', $UserId)
                            ->first();
        // $Blogs = Blogs::where('user_id', $UserId)
        //                     ->get();
        // echo json_encode($Tuitions);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.youtube_links', compact('YoutubeLinks'));
    }

    public function inquiries_list(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $Inquiries = Inquiries::where('user_id', $UserId)
                            ->get();

        // echo json_encode($Inquiries);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.inquiries', compact('Inquiries'));
    }

    public function templates(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        // $YoutubeLinks = YoutubeLinks::where('user_id', $UserId)
        //                     ->first();
        $TemplateSelection = TemplateSelection::where('user_id', $UserId)
                            ->first();
        // echo json_encode($Tuitions);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.templates', compact('TemplateSelection'));
    }

    public function store_or_update_banners(Request $request){
        $UserId = Session::get('TeacherId');

        $CheckBanners = Banners::where('user_id', $UserId)->first();

        if($CheckBanners){
            $Banners = Banners::where('user_id', $UserId)->first();

            $Banners->user_id = $UserId;

            if($request->hasfile('banner1')){
                $extension = $request->file('banner1')->getClientOriginalExtension();
                $dir = 'UI/banners/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('banner1')->move($dir, $filename1);

                $Banners->banner1 = $filename1;
            }else{
                $Banners->banner1 = $Banners->banner1;
            }

            if($request->hasfile('banner2')){
                $extension = $request->file('banner2')->getClientOriginalExtension();
                $dir = 'UI/banners/';
                $filename2 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('banner2')->move($dir, $filename2);

                $Banners->banner2 = $filename2;
            }else{
                $Banners->banner2 = $Banners->banner2;
            }

            if($request->hasfile('banner3')){
                $extension = $request->file('banner3')->getClientOriginalExtension();
                $dir = 'UI/banners/';
                $filename3 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('banner3')->move($dir, $filename3);

                $Banners->banner3 = $filename3;
            }else{
                $Banners->banner3 = $Banners->banner3;
            }


            $AddBanners = $Banners->save();

            // $CourseId = $request->session()->put('CourseId', $Courses->id);

            return redirect()->back()->with('message','Banner Added Successfully');
        }else{
            $Banners = new Banners();

            $Banners->user_id = $UserId;

            if($request->hasfile('banner1')){
                $extension = $request->file('banner1')->getClientOriginalExtension();
                $dir = 'UI/banners/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('banner1')->move($dir, $filename1);

                $Banners->banner1 = $filename1;
            }

            if($request->hasfile('banner2')){
                $extension = $request->file('banner2')->getClientOriginalExtension();
                $dir = 'UI/banners/';
                $filename2 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('banner2')->move($dir, $filename2);

                $Banners->banner2 = $filename2;
            }

            if($request->hasfile('banner3')){
                $extension = $request->file('banner3')->getClientOriginalExtension();
                $dir = 'UI/banners/';
                $filename3 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('banner3')->move($dir, $filename3);

                $Banners->banner3 = $filename3;
            }


            $AddBanners = $Banners->save();

            // $CourseId = $request->session()->put('CourseId', $Courses->id);

            return redirect()->back()->with('message','Banner Added Successfully');
        }


    }

    public function add_or_update_announcement(Request $request){
        $UserId = Session::get('TeacherId');

        $Users = Users::where('id', $UserId)->first();

        $Users->announcement = $request->announcement;

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','Announcement Updated Successfully');
    }

    public function update_profile(Request $request){
        $UserId = Session::get('TeacherId');

        $Users = Users::where('id', $UserId)->first();

        // $Users->name = $request->name;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;
        $Users->area = $request->area;

        if($request->hasfile('profile_pic')){
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $dir = 'UI/profile_pic/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('profile_pic')->move($dir, $filename);

            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }


        $Users->contact = $request->contact;
        $Users->qualification = $request->qualification;
        $Users->exp = $request->exp;
        $Users->about_me = $request->about_me;
        $Users->facebook = $request->facebook;
        $Users->twitter = $request->twitter;
        $Users->linkedin = $request->linkedin;
        $Users->instagram = $request->instagram;


        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Profile Updated Successfully');
    }


    public function store_or_update_tuitions(Request $request){
        $UserId = Session::get('TeacherId');

        $CheckTuitions = Tuitions::where('user_id', $UserId)->first();

        if($CheckTuitions){
            $Tuitions = Tuitions::where('user_id', $UserId)->first();

            $Tuitions->user_id = $UserId;
            $Tuitions->benefits_1 = $request->benefits_1;
            $Tuitions->benefits_2 = $request->benefits_2;
            $Tuitions->benefits_3 = $request->benefits_3;

            if($request->hasfile('tuition_img1')){
                $extension = $request->file('tuition_img1')->getClientOriginalExtension();
                $dir = 'UI/tuitions_img/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('tuition_img1')->move($dir, $filename1);

                $Tuitions->tuition_img1 = $filename1;
            }else{
                $Tuitions->tuition_img1 = $Tuitions->tuition_img1;
            }

            if($request->hasfile('tuition_img2')){
                $extension = $request->file('tuition_img2')->getClientOriginalExtension();
                $dir = 'UI/tuitions_img/';
                $filename2 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('tuition_img2')->move($dir, $filename2);

                $Tuitions->tuition_img2 = $filename2;
            }else{
                $Tuitions->tuition_img2 = $Tuitions->tuition_img2;
            }

            if($request->hasfile('tuition_img3')){
                $extension = $request->file('tuition_img3')->getClientOriginalExtension();
                $dir = 'UI/tuitions_img/';
                $filename3 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('tuition_img3')->move($dir, $filename3);

                $Tuitions->tuition_img3 = $filename3;
            }else{
                $Tuitions->tuition_img3 = $Tuitions->tuition_img3;
            }


            $AddTuitions = $Tuitions->save();

            // $CourseId = $request->session()->put('CourseId', $Courses->id);

            return redirect()->back()->with('message','Tuitions Updated Successfully');
        }else{
            $Tuitions = new Tuitions();

            $Tuitions->user_id = $UserId;
            $Tuitions->benefits_1 = $request->benefits_1;
            $Tuitions->benefits_2 = $request->benefits_2;
            $Tuitions->benefits_3 = $request->benefits_3;

            if($request->hasfile('tuition_img1')){
                $extension = $request->file('tuition_img1')->getClientOriginalExtension();
                $dir = 'UI/tuitions_img/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('tuition_img1')->move($dir, $filename1);

                $Tuitions->tuition_img1 = $filename1;
            }

            if($request->hasfile('tuition_img2')){
                $extension = $request->file('tuition_img2')->getClientOriginalExtension();
                $dir = 'UI/tuitions_img/';
                $filename2 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('tuition_img2')->move($dir, $filename2);

                $Tuitions->tuition_img2 = $filename2;
            }

            if($request->hasfile('tuition_img3')){
                $extension = $request->file('tuition_img3')->getClientOriginalExtension();
                $dir = 'UI/tuitions_img/';
                $filename3 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('tuition_img3')->move($dir, $filename3);

                $Tuitions->tuition_img3 = $filename3;
            }


            $AddTuitions = $Tuitions->save();

            // $CourseId = $request->session()->put('CourseId', $Courses->id);

            return redirect()->back()->with('message','Tuitions Added Successfully');
        }


    }


    // Blogs
    public function store_blogs(Request $request){
        $UserId = Session::get('TeacherId');

        $Blogs = new Blogs();

        $Blogs->user_id = $UserId;

        $Blogs->blog_title = $request->blog_title;

        if($request->hasfile('blog_img')){
            $extension = $request->file('blog_img')->getClientOriginalExtension();
            $dir = 'UI/blog_img/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('blog_img')->move($dir, $filename);

            $Blogs->blog_img = $filename;
        }

        $Blogs->date = $request->date;
        $Blogs->time = $request->time;
        $Blogs->description = $request->description;
        $Blogs->speaker = $request->speaker;

        $AddBlogs = $Blogs->save();

        return redirect()->back()->with('message','Blogs Added Successfully');
    }


    public function update_blogs(Request $request){
        $UserId = Session::get('TeacherId');

        $id = $request->id;

        $Blogs = Blogs::where('id', $id)->first();

        $Blogs->user_id = $UserId;

        $Blogs->blog_title = $request->blog_title;

        if($request->hasfile('blog_img')){
            $extension = $request->file('blog_img')->getClientOriginalExtension();
            $dir = 'UI/blog_img/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('blog_img')->move($dir, $filename);

            $Blogs->blog_img = $filename;
        }else{
            $Blogs->blog_img = $Blogs->blog_img;
        }

        $Blogs->date = $request->date;
        $Blogs->time = $request->time;
        $Blogs->description = $request->description;
        $Blogs->speaker = $request->speaker;

        $AddBlogs = $Blogs->save();

        return redirect()->back()->with('message','Blogs Updated Successfully');
    }

    // End

    public function add_or_update_yt_links(Request $request){
        $UserId = Session::get('TeacherId');

        $YoutubeLinks = YoutubeLinks::where('id', $UserId)->first();

        if($YoutubeLinks){
            $YoutubeLinks->user_id = $UserId;
            $YoutubeLinks->yt_link1 = $request->yt_link1;
            $YoutubeLinks->yt_link2 = $request->yt_link2;
            $YoutubeLinks->yt_link3 = $request->yt_link3;

            $AddYoutubeLinks = $YoutubeLinks->save();
        }else{
            $YoutubeLinks = new YoutubeLinks();

            $YoutubeLinks->user_id = $UserId;
            $YoutubeLinks->yt_link1 = $request->yt_link1;
            $YoutubeLinks->yt_link2 = $request->yt_link2;
            $YoutubeLinks->yt_link3 = $request->yt_link3;

            $AddYoutubeLinks = $YoutubeLinks->save();
        }



        return redirect()->back()->with('message','Youtube Links Updated Successfully');
    }


    public function change_password(){
        $title ="Change Password";
        $UserId = Session::get('AdminId');
        // $RequestCallBack = RequestCallBack::where('user_id', $UserId)->get();
        return view('teachers.change_password', compact('title'));
    }


    public function update_password(Request $request)
    {

        //  $this->validate($request, [

        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);

        $UserId = Session::get('TeacherId');

       $hashedPassword = Auth::guard('teacher')->user()->password;

    //    if (\Hash::check($request->oldpassword , $hashedPassword )) {

         if (!\Hash::check($request->newpassword , $hashedPassword)) {

              $users =Users::find($UserId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $UserId)->update( array( 'password' =>  $users->password));

              session()->flash('message','password updated successfully');
              return redirect()->back();
            }

            else{
                  session()->flash('message','new password can not be the old password!');
                  return redirect()->back();
                }

        //    }

        //   else{
        //        session()->flash('message','old password doesnt matched ');
        //        return redirect()->back();
        //      }

       }
}
