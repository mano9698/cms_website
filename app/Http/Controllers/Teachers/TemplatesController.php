<?php

namespace App\Http\Controllers\Teachers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teachers\Banners;
use App\Models\Users;
use App\Models\Teachers\Tuitions;
use App\Models\Teachers\Blogs;
use App\Models\Teachers\YoutubeLinks;
use App\Models\Teachers\Inquiries;
use App\Models\Teachers\TemplateSelection;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class TemplatesController extends Controller
{
    public function contact($name){
        $title = "Teachers Dashboard";

        $GetId = Users::where('slug', $name)->first();
        
        $UserId = $GetId->id;

        $Users = Users::where('id', $UserId)
                            ->first();

        // $UserId = Session::get('TeacherId');  
        // $Courses = Courses::where('teacher_id', $UserId)
        //                     ->get();

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        return view('teachers.templates.contact', compact('Users'));
    }

    public function template_preview($id){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');  

        $Users = Users::where('id', $UserId)
                            ->first();

        $Banners = Banners::where('user_id', $UserId)
                            ->first();
        
        $Tuitions = Tuitions::where('user_id', $UserId)
                    ->first();

        $Blogs = Blogs::where('user_id', $UserId)
                            ->take(4)->get();

        $YoutubeLinks = YoutubeLinks::where('user_id', $UserId)
                    ->first();

        // echo json_encode($Blogs);
        // exit;
        $TemId = $id;

        if($TemId == 1){
            return view('teachers.templates.template1', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($TemId == 2){
            return view('teachers.templates.template2', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($TemId == 3){
            return view('teachers.templates.template3', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($TemId == 4){
            return view('teachers.templates.template4', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($TemId == 5){
            return view('teachers.templates.template5', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($TemId == 6){
            return view('teachers.templates.template6', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }

        
    }

    public function templates($name){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');  
        $GetId = Users::where('slug', $name)->first();
        
        $UserId = $GetId->id;

        $Users = Users::where('id', $UserId)
                            ->first();

        $Banners = Banners::where('user_id', $UserId)
                            ->first();
        
        $Tuitions = Tuitions::where('user_id', $UserId)
                    ->first();

        $Blogs = Blogs::where('user_id', $UserId)
                            ->take(4)->get();

        $YoutubeLinks = YoutubeLinks::where('user_id', $UserId)
                    ->first();

        // echo json_encode($Blogs);
        // exit;

        // $Groups = Groups::where('teacher_id', $UserId)
        //             ->count();
        $CheckTemplateSelection = TemplateSelection::where('user_id', $UserId)->first();

        if($CheckTemplateSelection->template_id == 1){
            return view('teachers.templates.template1', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($CheckTemplateSelection->template_id == 2){
            return view('teachers.templates.template2', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($CheckTemplateSelection->template_id == 3){
            return view('teachers.templates.template3', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($CheckTemplateSelection->template_id == 4){
            return view('teachers.templates.template4', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($CheckTemplateSelection->template_id == 5){
            return view('teachers.templates.template5', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }elseif($CheckTemplateSelection->template_id == 6){
            return view('teachers.templates.template6', compact('Users', 'Banners', 'Tuitions', 'Blogs', 'YoutubeLinks'));
        }

        
    }

    public function add_contact(Request $request){
        // $UserId = Session::get('TeacherId');  

        $Inquiries = new Inquiries();

        $Inquiries->user_id = $request->user_id;
        $Inquiries->name = $request->name;
        $Inquiries->email = $request->email;
        $Inquiries->contact = $request->contact;
        $Inquiries->whatsapp_no = $request->whatsapp_no;
        $Inquiries->country = $request->country;
        $Inquiries->state_city = $request->state_city;
        $Inquiries->requirements = $request->requirements;

        $AddInquiries = $Inquiries->save();

        return redirect()->back()->with('message','Inquiries Updated Successfully');
    }

    public function add_template_selection(Request $request){
        $UserId = Session::get('TeacherId');
        
        // echo $request->template_id;
        // exit;

        $CheckTemplateSelection = TemplateSelection::where('user_id', $UserId)->first();

        if($CheckTemplateSelection){
            $CheckTemplateSelection->user_id = $UserId;
            $CheckTemplateSelection->template_id = $request->template_id;
            $CheckTemplateSelection->save();

            return redirect()->back()->with('message','Templates Updated Successfully');
        }else{
            $TemplateSelection = new TemplateSelection();

            $TemplateSelection->user_id = $UserId;
            $TemplateSelection->template_id = $request->template_id;
            $TemplateSelection->save();

            return redirect()->back()->with('message','Templates Updated Successfully');
        }
    }


    public function blogs_list($name){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');  
        $GetId = Users::where('slug', $name)->first();
        
        $UserId = $GetId->id;

        $Blogs = Blogs::where('user_id', $UserId)
                            ->get();

        $Users = Users::where('id', $UserId)
                    ->first();

        return view('teachers.templates.blogs_list', compact('Blogs', 'Users'));        
    }


    public function blogs_details($id){
        $title = "Teachers Dashboard";
        // $UserId = Session::get('TeacherId');  
        // $GetId = Users::where('slug', $name)->first();
        
        // $UserId = $GetId->id;

        $Blogs = Blogs::where('id', $id)
                            ->first();

        $Users = Users::where('id', $Blogs->user_id)
                    ->first();

        $RecentBlogs = Blogs::where('user_id', '!=', $Blogs->user_id)
                    ->inRandomOrder()->take(3)->get();

        return view('teachers.templates.blog_details', compact('Blogs', 'Users', 'RecentBlogs'));        
    }
}
