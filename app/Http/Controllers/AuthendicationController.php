<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class AuthendicationController extends Controller
{
    public function login(){
        return view('login');
    }

    public function register(){
        return view('register');
    }

    public function add_users(Request $request){
        $Users = new Users();

        $Users->name = $request->name;
        $Users->slug = strtolower($request->name);
        $Users->email = $request->email;
        $Users->password = Hash::make($request->password);
        $Users->contact = $request->contact;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;
        $Users->area = $request->area;
        $Users->status = 0;
        $Users->user_type = 2;

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Registered Successfully');
    }


    public function admin_login(Request $request){
        
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        // $request->session()->put('last_login_timestamp', time());

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if($CheckEmail->status == 0){

                return redirect()->back()->with('message','Your account is not activated. Please contact your administrator...');
                    
            }elseif($CheckEmail->user_type == 1){
                if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                    $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }elseif($CheckEmail->user_type == 2){
                if (Auth::guard('teacher')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Teachername', Auth::guard('teacher')->user()->name);
                    $request->session()->put('Teacherslug', Auth::guard('teacher')->user()->slug);
                    $request->session()->put('TeacherEmail', Auth::guard('teacher')->user()->email);
                    $request->session()->put('TeacherId', Auth::guard('teacher')->user()->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('teachers/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }
        }
        
    }

    public function teacher_logout()
    {
        Auth::guard('teacher')->logout();
        return redirect('/');
    }

    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        return redirect('/');
    }
}
